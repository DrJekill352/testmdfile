# How run e2e test in internet explorer #

* Globally update protractor:
```node
	npm install protractor -g
```
* Delete "directConnect: false" in protractor.conf.js:
```javascript
    directConnect: true,
```
* Add "seleniumAddress: 'http://localhost:4444/wd/hub'" in protractor.conf.js:
```javascript
    seleniumAddress: 'http://localhost:4444/wd/hub',
```
* Edit browser name on 'internet explorer':
```javascript
    capabilities: {
        'browserName': 'internet explorer',
    },
```
* Update webdriver-manager for internet explorer:
```node
	webdriver-manager update --ie
```
* Start webdriver-manager for internet explorer:
```node
	webdriver-manager start --ie
```
* Start tests